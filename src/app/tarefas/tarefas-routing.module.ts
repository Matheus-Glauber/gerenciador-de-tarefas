import { EditarTarefaComponent } from './editar-tarefa/editar-tarefa.component';
import { CadastrarTarefaComponent } from './cadastrar-tarefa/cadastrar-tarefa.component';
import { Routes } from '@angular/router';
import { ListarTarefaComponent } from './listar-tarefa/listar-tarefa.component';

export const TarefaRoutes: Routes = [
  {
    path: 'tarefas/listar',
    redirectTo: 'listar-tarefa/listar-tarefa.component'
  },
  {
    path: 'listar-tarefa/listar-tarefa.component',
    component: ListarTarefaComponent
  },
  {
    path: 'tarefas/cadastrar',
    component: CadastrarTarefaComponent
  },
  {
    path: 'tarefas/editar/:id',
    component: EditarTarefaComponent
  }
]
